from flask import Flask, render_template, jsonify, request
import json
import requests

app = Flask(__name__)

@app.route("/")
def map():
	file_object = open("/root/lboard/data.json", "r")
	installs = json.load(file_object)
	installs_total = len(installs)
	file_object.close()
	return render_template("map.html", installs_total=installs_total)

@app.route("/data")
def data():
	file_object = open("/root/lboard/data.json", "r")
	installs = json.load(file_object)
	file_object.close()
	return json.dumps(installs)

if __name__ == "__main__":
	app.run(debug=True)
