import json
import requests
import geocoder
import pytz
import datetime

contacts_api = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
accounts_api = "https://levelsolar.secure.force.com/api/services/apexrest/accounts"
cases_api = "https://levelsolar.secure.force.com/api/services/apexrest/cases"

def json_to_date():
  page_num = 1
  cases = []
  data = []
  case_param = {"type_name": "Install", "status": "Installed as designed"}
  response = requests.get(cases_api, params=case_param)
  page_list = json.loads(response.text)
  cases.extend(page_list)
  while len(page_list) == 100:
    page_num = page_num + 1
    new_param = {"type_name": "Install", "status": "Installed as designed", "page": page_num}
    response = requests.get(cases_api, params=new_param)
    page_list = json.loads(response.text)
    cases.extend(page_list)
  print len(cases)

def json_to_date_2():
  page_num = 1
  cases = []
  data = []
  case_param = {"type_name": "Install"}
  response = requests.get(cases_api, params=case_param)
  page_list = json.loads(response.text)
  cases.extend(page_list)
  while len(page_list) == 100:
    page_num = page_num + 1
    new_param = {"type_name": "Install", "page": page_num}
    response = requests.get(cases_api, params=new_param)
    page_list = json.loads(response.text)
    cases.extend(page_list)
  for item in cases:
    if "status" in item:
      if item["status"] == "Installed as designed":
        data.append(item)
  print len(data)

json_to_date()
json_to_date_2()