import json
import requests
import geocoder
import pytz
import datetime

contacts_api = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
accounts_api = "https://levelsolar.secure.force.com/api/services/apexrest/accounts"
cases_api = "https://levelsolar.secure.force.com/api/services/apexrest/cases"


# time definitions
utc_zone = pytz.timezone('UTC')
est_zone = pytz.timezone('US/Eastern')
now_utc_naive = datetime.datetime.utcnow()
now_utc_aware = utc_zone.localize(now_utc_naive)
now_est_aware = now_utc_aware.astimezone(est_zone)
today_12am_est = now_est_aware.replace(hour=0, minute=0, second=0, microsecond=0)
today_12am_est_in_utc = today_12am_est.astimezone(utc_zone)
one_day = datetime.timedelta(days=1)
today_12am_est_in_utc_minus_30_days = today_12am_est_in_utc - (one_day * 30)
today_12am_est_in_utc_minus_30_days_naive = today_12am_est_in_utc_minus_30_days.replace(tzinfo=None)
thirty_days_ago = today_12am_est_in_utc_minus_30_days_naive.isoformat()

def json_to_date():
  cases = []
  data = []
  case_param = {"type_name": "Install", "ordering": "createddate"}
  response = requests.get(cases_api, params=case_param)
  page_list = json.loads(response.text)
  cases.extend(page_list)
  while len(page_list) > 0:
    new_param = {"type_name": "Install", "createddate__gt": page_list[-1]["createddate"]}
    response = requests.get(cases_api, params=new_param)
    page_list = json.loads(response.text)
    cases.extend(page_list)
  for item in cases:
    if "status" in item:
      if item["status"] == "Installed as designed" or item["status"] == "Installed different than design":
        address = item["account"]["name"]
        point = geocoder.google(address)
        if " - " in address:
          if len(address.split(" - ")) == 3:
            street_address, city, zip_code = address.split(" - ")
          else:
            street_address = "Bad Data"
            city = "Bad Data"
            zip_code = "Bad Data"
        else:
          street_address = "Bad Data"
          city = "Bad Data"
          zip_code = "Bad Data"
        if point.latlng:
          lat_lng = point.latlng
          lat = lat_lng[0]
          lng = lat_lng[1]
        account_id = item["account"]["id"]
        contact_param = {"account": account_id}
        r = requests.get(contacts_api, params=contact_param)
        contact_list = json.loads(r.text)
        contact = contact_list[0]
        name = contact["name"]
        data_point = {"geometry": {"type": "Point", "coordinates": [lng, lat]},
                      "type": "Feature",
                      "properties": {"name": name, "street_address": street_address, "city": city, "zip": zip_code, "id": account_id}}
        data.append(data_point)
  file_object = open("/root/lboard/data.json", "w")
  json.dump(data, file_object)
  file_object.close()

def add_to_json():
  install_ids = []
  file_object = open("/root/lboard/data.json", "r")
  installs = json.load(file_object)
  file_object.close()
  for item in installs:
    install_ids.append(item["properties"]["id"])
  cases = []
  new_installs = []
  case_param = {"type_name": "Install", "scheduled_install_date__gte": str(thirty_days_ago), "ordering": "createddate"}
  response = requests.get(cases_api, params=case_param)
  page_list = json.loads(response.text)
  cases.extend(page_list)
  while len(page_list) > 0:
    new_param = {"type_name": "Install", "createddate__gt": page_list[-1]["createddate"]}
    response = requests.get(cases_api, params=new_param)
    page_list = json.loads(response.text)
    cases.extend(page_list)
  for item in cases:
    if "status" in item:
      if item["status"] == "Installed as designed" or item["status"] == "Installed different than design":
        if item["account"]["id"] not in install_ids:
          address = item["account"]["name"] 
          point = geocoder.google(address)
          if " - " in address:
            if len(address.split(" - ")) == 3:
              street_address, city, zip_code = address.split(" - ")
            else:
              street_address = "Bad Data"
              city = "Bad Data"
              zip_code = "Bad Data"
          else:
            street_address = "Bad Data"
            city = "Bad Data"
            zip_code = "Bad Data"
          if point.latlng:
            lat_lng = point.latlng
            lat = lat_lng[0]
            lng = lat_lng[1]
          account_id = item["account"]["id"]
          contact_param = {"account": account_id}
          r = requests.get(contacts_api, params=contact_param)
          contact_list = json.loads(r.text)
          contact = contact_list[0]
          name = contact["name"]
          data_point = {"geometry": {"type": "Point", "coordinates": [lng, lat]},
                       "type": "Feature",
                        "properties": {"name": name, "street_address": street_address, "city": city, "zip": zip_code}}
          new_installs.append(data_point)
  installs.extend(new_installs)
  file_object = open("/root/lboard/data.json", "w")
  json.dump(installs, file_object)
  file_object.close()

add_to_json()